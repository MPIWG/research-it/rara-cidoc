#!/bin/sh

# pass folder as first argument
# pass output as second argument

DIRECTORY=$1
OUTPUT=$2

touch $OUTPUT

find $DIRECTORY -type f -name "*.ttl" \
 | while read f ; do
     cat "$f" >> $OUTPUT.txt
   done

mv $OUTPUT.txt $OUTPUT