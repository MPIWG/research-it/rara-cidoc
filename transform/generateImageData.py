from os import listdir
from rdflib import Graph
import re
import sys


# Script expects a ttl file as first argument
ttlFile = sys.argv[1]

# Parse the turtle file into a graph
g = Graph()
g.parse(ttlFile, format='n3')

# Query for the information carrier that carries the jpegs
gres = g.query("""
    SELECT ?s ?id ?original WHERE
        {
            ?s a frbroo:E84_Information_Carrier ;
                crm:P3_has_note ?id .
            ?original frbroo:R29i_was_reproduced_by/frbroo:R30_produced ?s .
            FILTER(REGEX(STR(?s), "content.mpiwg-berlin.mpg.de"))
        }""")

for carrier in gres:

    # Derive directory from URI, assuming online_permanent is mounted as volume
    directory = str(carrier.s).replace('http://content.mpiwg-berlin.mpg.de/mpiwg/online/permanent/', '/Volumes/online_permanent/')

    # Identify directory with page images (most of the time pageimg)
    pageimg = [s for s in listdir(directory) if "pageimg" in s]

    if len(pageimg) :
        pagesDir = directory + '/' + pageimg[0]
        pages = listdir(pagesDir)
        pages.sort()
        # Generate triples for each image
        for i, page in enumerate(pages):
            uri = '<' + str(carrier.s) + '/' + pageimg[0] + '/' + page + '>'
            print( uri + ' a crm:E38_Image . ' )
            print( uri + ' crm:P1_is_identified_by ' + '"' + str(i) + '" .')
            print( '<' + str(carrier.s) + '> crm:P128_carries ' + uri + ' .')
            if str(i) == str(carrier.id):
                # Assign representative image
                print( '<' + str(carrier.original) + '> crm:P138i_has_representation ' + uri + ' .')
