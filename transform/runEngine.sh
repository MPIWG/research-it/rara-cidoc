#!/bin/bash

# Takes thre arguments
# first: input folder of xml files
# second: output folder for turtle files
# third: path to generator xml

INPUTFOLDER=$1
OUTPUTFOLDER=$2
GENERATORFILE=$3

# The ID refers to the ID of the mapping created in 3MEditor
ID=3

if [ "$#" -ne 3 ]; then
    echo "Received wrong number of parameters"
    exit;
fi


CURRENTFOLDER=$(PWD)
EGINEURLCONF="$CURRENTFOLDER/engineURL.txt"
IDCONF="$CURRENTFOLDER/mappingID.txt"

if [ ! -f $EGINEURLCONF ]; then
    echo "Enter url the x3ml Engine (e.g. http://localhost:7780/x3mlMapper):"
    read ENGINEURL
    echo $ENGINEURL > $EGINEURLCONF
    echo "Path stored in $EGINEURLCONF"
else
  ENGINEURL=$(<$EGINEURLCONF)
fi

if [ ! -f $IDCONF ]; then
    echo "Enter the id of the mapping:"
    read ID
    echo $ID > $IDCONF
    echo "ID stored in $IDCONF"
else
  ID=$(<$IDCONF)
fi

for f in $INPUTFOLDER/*.xml
do
  o="$OUTPUTFOLDER/$(echo $f | sed 's/\.xml/\.ttl/')"
  curl -X POST \
    --no-keepalive \
    --data-urlencode "sourceFile=$(cat $f)" \
    --data-urlencode "generator=$(cat $GENERATORFILE)" \
    -d "id=$ID" \
    -d 'uuidSize=2' \
    -d 'output=Turtle' \
    -o $o \
    $ENGINEURL/Index
done
mv $INPUTFOLDER/*.ttl $OUTPUTFOLDER
