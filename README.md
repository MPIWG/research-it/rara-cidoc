## About

This repository contains models for mapping the MPIWG's rara collection into CIDOC-CRM/RDF.

It is maintained by the MPIWG library and research IT

## What's Inside

- examples/
    - index.meta input files as examples
- transform/
    - scripts to apply the mappings to the index.meta files 
- rara_im2cidoc.ttl
    - a (manually created) example of a CIDOC-CRM output record
- rara_protege.ttl
    - the same as rara_im2cidoc.ttl but with ontology import and some additional content so the file can be more easily opened in Protégé

## Usage

### runEngine.sh

This script runs a batch conversion of XML files against a mapping defined in a 3M Editor instance running on a server.

It takes three arguments:
1. an input folder that contains XML files to be converted
2. an output folder where the converted TTL files are stored
3. a path to the generator XML file

```
$ bash runEngine.sh ../examples ../output ../mapping/generator.xml
```

On first run, the script prompts for the URL to the server that runs the instance of the 3M editor. It also prompts for the ID number of the mapping that should be applied. 
Both parameterss are stored locally and retrieved automatically upon subsequent runs. To apply a new mapping or use a mapping from another server, delete the configuration files.

### generateImageData.py

As the index.meta do not contain any metadata on the digitised images, this information needs to be retrieved from the server.

The script takes the path to a Turtle file as an argument and then generates the metadata based on the content of the respective directory. 
The script assumes that the server folder was mounted before execution. Output goes to stdout.

```
$ python generateImageData.py input.ttl > output.ttl
```